package dataStructures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class HashTableAsgmnt {

	public static void main(String[] args) {

		// ● Create a HashMap for the product price page where all products will be as
		// the key and the price as the value.
		Map<String, Double> products = new Hashtable<String, Double>();

		// ● Save at least 8 products in the Hashtable.
		products.put("Bananas", 1.20);
		products.put("Cinnamon", 4.30);
		products.put("Oil", 7.60);
		products.put("Flour", 0.90);
		products.put("Salt", 0.20);
		products.put("Mango", 5.50);
		products.put("Olives", 10.00);
		products.put("Coffee", 21.00);
		products.put("Milk", 1.30);

		// ● Find out how many products are under 1 eur.

		int countUnder1 = 0;

		Iterator<Double> iterator = products.values().iterator();
		while (iterator.hasNext()) {
			Double currentElement = iterator.next();
			if (currentElement < 1)
				countUnder1++;
		}
		System.out.println("There are " + countUnder1 + " elements under 1 eur.");

		// ● Find out which product price is the lowest.

		Double min = Collections.min(products.values());
		System.out.println("The lowest product price is " + min + " Eur.");

		// ● Find out which product price is the largest.
		Double max = Collections.max(products.values());
		System.out.println("The highest product price is " + max + " Eur.");

		// ● Create a new Hashtable with other products.
		Map<String, Double> products2 = new Hashtable<String, Double>();

		products2.put("Soy milk", 2.20);
		products2.put("Cat food", 8.50);
		products2.put("Salmon", 12.30);
		products2.put("Pepper", 1.90);
		products2.put("Candy", 1.00);

		// Merge both Hashtables and print out how many products are in the merged
		// Hashtable

		products.putAll(products2);

		System.out.println("There are " + products.size() + " products in the merged Hashtable.");

		// ● *Sort all values in ascending order.

		List<Double> mapValues = new ArrayList<>(products.values());

		Collections.sort(mapValues);

		System.out.print("All values in ascending order: " + mapValues);

	}

}
