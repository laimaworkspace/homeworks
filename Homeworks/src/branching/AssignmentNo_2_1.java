package branching;

import java.util.Scanner;

public class AssignmentNo_2_1 {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Enter the number of the day in the week:");
		int number = myScanner.nextInt();

		
		switch (number) {
		case 1:
			System.out.println("It is a working day");
			break;
		case 2:
			System.out.println("It is a working day");
			break;
		case 3:
			System.out.println("It is a working day");
			break;
		case 4:
			System.out.println("It is a working day");
			break;
		case 5:
			System.out.println("It is a working day");
			break;
		case 6:
			System.out.println("It is holiday");
			break;
		case 7:
			System.out.println("It is holiday");
			break;
			default:
		System.out.println("Day number is incorrect");
			return;
			
		
		}
		
		myScanner.close();
	}

}
