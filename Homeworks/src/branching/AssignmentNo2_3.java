package branching;

import java.util.Scanner;

public class AssignmentNo2_3 {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		

		System.out.println("Enter the first number:");
		double number1 = Double.parseDouble(myScanner.nextLine());


		System.out.println("Enter option +, -, /, *, %, p, b or s:");
		char option = myScanner.nextLine().charAt(0);

		System.out.println("Enter the second number:");
		double number2 = Double.parseDouble(myScanner.nextLine());
		
		double result = 0;
		
		switch (option) {
		case '+':
			result = number1 + number2;
			System.out.println("The result is " + result);
			break;
		case '-':
			result = number1 - number2;
			System.out.println("The result is " + result);
			break;
		case '/':
			result = number1 / number2;
			System.out.println("The result is " + result);
			break;
		case '%':
			result = number1 % number2;
			System.out.println("The result is " + result);
			break;
		case 'p':
			System.out.println((number1));
			System.out.println((number2));
			break;
		case 'b':
			if (number1 > number2) 
				System.out.println(number1 + " is bigger than " + number2);
			else if (number1 < number2) 
				System.out.println(number2 + " is bigger than " + number1);
			else 
				System.out.println("Both numbers are the same");
			return;
			
		case 's':
			if (number1 < number2) 
				System.out.println(number1 + " is smaller than " + number2);
			else if (number1 > number2) 
				System.out.println(number2 + " is smaller than " + number1);
			else 
				System.out.println("Both numbers are the same");
			return;
		
		}
	
}
}
