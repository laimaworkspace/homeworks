package branching;

import java.util.Scanner;

public class AssignmentNo2_2 {

	public static void main(String[] args) {
		
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Enter the grade level of the exam:");
		char grade = myScanner.next().charAt(0);

		
		switch (grade) {
		case 'A':
			System.out.println("Perfect! You are so clever!");
			break;
		case 'B':
			System.out.println("Perfect! You are so clever!");
			break;
		case 'C':
			System.out.println("Good! But You can do better!");
			break;
		case 'D':
			System.out.println("It is not good! You should study!");
			break;
		case 'E':
			System.out.println("It is not good! You should study!");
			break;
		case 'F':
			System.out.println("Fail! You need to repeat the exam!");
			break;
			default:
				System.out.println("The grade level is incorrect.");
				return;

	}
		myScanner.close();
}
}

