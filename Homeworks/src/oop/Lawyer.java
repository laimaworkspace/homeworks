package oop;

public class Lawyer extends Person {

	private String name, surname;
	private int lawyerID, helpedInCrimesSolving;

	public Lawyer() {

	}

	public Lawyer(String name, String surname, int lawyerID, int helpedInCrimesSolving) {
		this.name = name;
		this.surname = surname;
		this.lawyerID = lawyerID;
		this.helpedInCrimesSolving = helpedInCrimesSolving;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getLawyerID() {
		return lawyerID;
	}

	public void setLawyerID(int lawyerID) {
		this.lawyerID = lawyerID;
	}

	public int getHelpedInCrimesSolving() {
		return helpedInCrimesSolving;
	}

	public void setHelpedInCrimesSolving(int helpedInCrimesSolving) {
		this.helpedInCrimesSolving = helpedInCrimesSolving;
	}

	@Override
	public String toString() {
		return "Name: " + this.name + System.lineSeparator() + "Surname: " + this.surname + System.lineSeparator()
				+ "Lawyer ID: " + this.lawyerID + System.lineSeparator() + "Helped to solve crimes "
				+ this.helpedInCrimesSolving + " times";

	}

}
