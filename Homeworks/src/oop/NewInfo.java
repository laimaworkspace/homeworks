package oop;

import java.util.ArrayList;
import java.util.Iterator;

public class NewInfo {

	public static void main(String[] args) {

		/// a. Create seven Officers as the objects, two Districts as the objects and
		/// three Lawyers as the objects:

		Officer[] officers = new Officer[7];
		for (int i = 0; i < 7; i++)
			officers[i] = new Officer();

		Lawyer lawyer1 = new Lawyer("Marija", "Jekabsone", 11, 120);
		Lawyer lawyer2 = new Lawyer("Linda", "Karklina", 21, 26);
		Lawyer lawyer3 = new Lawyer("Zane", "Ziedina", 3, 195);

		District district1 = new District("District22", "Riga", 124);
		District district2 = new District("District44", "Liepaja", 1246);

		/// b. Add three Officers in the first District and others in the second
		/// District.

		for (int i = 0; i < 3; i++)
			district1.addNewOfficer(officers[i]);

		for (int i = 3; i < 7; i++)
			district2.addNewOfficer(officers[i]);

		/// c. Print out all information about each District.
		System.out.println(district1);
		System.out.print(System.lineSeparator());
		System.out.println(district2);

		/// d. Print out information about all Lawyers.
		System.out.print(System.lineSeparator());
		System.out.println(lawyer1);
		System.out.print(System.lineSeparator());
		System.out.println(lawyer2);
		System.out.print(System.lineSeparator());
		System.out.println(lawyer3);
		System.out.print(System.lineSeparator());

		/// e. Create an arraylist for Lawyers storing. Put all Lawyers in it.
		ArrayList<Lawyer> list = new ArrayList<Lawyer>();
		list.add(lawyer1);
		list.add(lawyer2);
		list.add(lawyer3);

//// f. Find out the total number of the crimes in which solving the Lawyers
//// were involved.

		Iterator<Lawyer> iterator = list.iterator();
		int totalCrimes = 0;
		while (iterator.hasNext()) {
			Lawyer lawyer = iterator.next();
			totalCrimes += lawyer.getHelpedInCrimesSolving();
		}
		System.out.println("The total number of the crimes solved by lawyers: " + totalCrimes);

		//// g. Find out which Lawyer has helped the most to solve crimes.

		Lawyer mostCrimesLawyer = null;
		Iterator<Lawyer> iter = list.iterator();
		while (iter.hasNext()) {
			Lawyer currentLawyer = iter.next();
			if (mostCrimesLawyer == null
					|| mostCrimesLawyer.getHelpedInCrimesSolving() < currentLawyer.getHelpedInCrimesSolving())
				mostCrimesLawyer = currentLawyer;
		}
		System.out.println(mostCrimesLawyer.getName() + " is the lawyer who has helped solve crimes the most times.");
	}

}
