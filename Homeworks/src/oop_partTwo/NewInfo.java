package oop_partTwo;

import java.util.ArrayList;

public class NewInfo {

	public static void main(String[] args) {

		// a. Create seven Officers as the objects, two Districts as the objects and
		// three Lawyers as the objects.

		Officer officer1 = new Officer("Maris", "Liepa", "District1", 1, 65);
		Officer officer2 = new Officer("Zigmunds", "Ozols", "District1", 2, 68);
		Officer officer3 = new Officer("Nauris", "Zakis", "District1", 3, 5);
		Officer officer4 = new Officer("Maija", "Zeltina", "District2", 4, 90);
		Officer officer5 = new Officer("Anda", "Bodniece", "District2", 5, 32);
		Officer officer6 = new Officer("Edgars", "Kalnins", "District2", 6, 61);
		Officer officer7 = new Officer("Peteris", "Klava", "District2", 7, 56);

		Lawyer lawyer1 = new Lawyer("Marija", "Jekabsone", 11, 120);
		Lawyer lawyer2 = new Lawyer("Linda", "Karklina", 21, 26);
		Lawyer lawyer3 = new Lawyer("Zane", "Ziedina", 3, 195);

		District district1 = new District("District1", "Riga", 124);
		District district2 = new District("District2", "Liepaja", 1246);

		// b. Add three Officers and two Lawyers in the first District and others
		// Officers and Lawyers in the second District.

		district1.addNewPerson(officer1);
		district1.addNewPerson(officer2);
		district1.addNewPerson(officer3);
		district1.addNewPerson(lawyer1);
		district1.addNewPerson(lawyer2);

		district2.addNewPerson(lawyer3);
		district2.addNewPerson(officer4);
		district2.addNewPerson(officer5);
		district2.addNewPerson(officer6);
		district2.addNewPerson(officer7);

		// c. Print out all information about each District.

		System.out.println(district1);
		System.out.print(System.lineSeparator());
		System.out.println(district2);
		System.out.print(System.lineSeparator());

		// d. Edit the method calculateAvgLevelInDistrict()
		// that only all Officers� levels are summarized and divided by the amount of
		// the Officers (not Lawyers) in this District.

		District district = new District();
		district.getPersonsInTheDistrict().addAll(district1.getPersonsInTheDistrict());
		district.getPersonsInTheDistrict().addAll(district2.getPersonsInTheDistrict());

		System.out.println("The average level for officers is: " + district.calculateAvgLevellInDistrict());

		// e. Create an arraylist for the Districts storage. Put both Districts in the
		// arraylist.

		ArrayList<District> districts = new ArrayList<District>();

		districts.add(district1);
		districts.add(district2);

		// f. Find out which District is with the highest amount of Persons.

		if (district1.getPersonsInTheDistrict().size() > district2.getPersonsInTheDistrict().size()) {
			System.out.println("District1 has highest amount of persons");
		} else if (district1.getPersonsInTheDistrict().size() < district2.getPersonsInTheDistrict().size()) {
			System.out.println("District2 has highest amount of persons");
		} else
			System.out.println("Both districts have same amount of persons");

	}

}
