package oop_partTwo;

import java.util.ArrayList;
import java.util.Iterator;

public class District {

	private String title, city;
	private int districtID;

	private ArrayList<Person> personsInTheDistrict = new ArrayList<Person>();

	public District(String title, String city, int districtID) {
		this.city = city;
		this.districtID = districtID;
		this.title = title;
		this.city = city;
	}

	public District() {
		// TODO Auto-generated constructor stub
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getDistrictID() {
		return districtID;
	}

	public void setDistrictID(int districtID) {
		this.districtID = districtID;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "The title:" + this.title + System.lineSeparator() + "City: " + this.city + System.lineSeparator()
				+ "District ID: " + this.districtID + System.lineSeparator() + "There are "
				+ this.personsInTheDistrict.size() + " persons in the district";
	}

	public void addNewPerson(Person person) {
		this.personsInTheDistrict.add(person);
	}

	public void removePerson(Person person) {
		this.personsInTheDistrict.remove(person);
	}

	public float calculateAvgLevellInDistrict() {
		float levelSum = 0;
		float numberOfOfficers = 0;

		// d:Edit the method calculateAvgLevelInDistrict()
		// that only all Officers� levels are summarized and divided by the amount of
		// the Officers (not Lawyers) in this District.

		Iterator<Person> iterator = this.personsInTheDistrict.iterator();
		while (iterator.hasNext()) {

			Person person = iterator.next();
			if (person instanceof Officer) {
				Officer officer = (Officer) person;
				numberOfOfficers++;
				levelSum += officer.calculateLevel();
			}
		}
		return (float) levelSum / (float) numberOfOfficers;

	}

	public ArrayList<Person> getPersonsInTheDistrict() {
		return personsInTheDistrict;
	}

	public void setPersonsInTheDistrict(ArrayList<Person> personsInTheDistrict) {
		this.personsInTheDistrict = personsInTheDistrict;
	}

}
